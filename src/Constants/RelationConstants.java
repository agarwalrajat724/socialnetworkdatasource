package Constants;

public interface RelationConstants {
    String FRIEND = "isFriendOf";

    String WORK = "worksAt";

    String STAY = "staysIn";

    String STUDY = "studiedAt";

    String PLAY = "plays";
}
