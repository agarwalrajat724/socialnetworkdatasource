import static Constants.RelationConstants.*;

import enums.RelationEnum;
import helper.UploadData;
import model.Person;
import model.Predicate;
import model.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        UploadData.class
})
public class TestJunit {

    private BufferedReader bufferedReader = null;


    private static final String SELECT = "SELECT";

    private static final String WHERE = "WHERE";

    @Before
    public void setUp() {
        try {
            bufferedReader = new BufferedReader(new FileReader("/home/raagar/Documents/DataSource/Query.txt"));
            System.out.println(UploadData.persons);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void query() {
        try {
            String query = null;
            List<Query> queries = new ArrayList<>();
            while ((query = bufferedReader.readLine()) != null) {
                String upperQuery = query.toUpperCase();
                int indexOfWhere = upperQuery.indexOf(WHERE);
                int indexOfSelect = upperQuery.indexOf(SELECT);

                if (indexOfSelect == -1) {
                    System.out.println("Error... No Select clause in the Query...");
                    return;
                }

                if (indexOfWhere == -1) {
                    System.out.println("Error... No Where clause in the Query...");
                    return;
                }

                String[] projections = query.substring(SELECT.length(), indexOfWhere).trim().split(",");
                List<String> projects = new ArrayList<>();
                if (null != projections && projections.length > 0) {
                    for (int i = 0; i < projections.length; i++) {
                        String pr = projections[i].trim();
                        projects.add(pr.substring(1, pr.length()));
                    }
                }

                String[] predicates = query.substring(indexOfWhere + WHERE.length()).split(",");

                Query queryModel = new Query();
                Predicate predicate = null;
                List<Predicate> predicateList = new ArrayList<>();
                for (int i = 0; i < predicates.length; i++) {
                    String[] currentPredicate = predicates[i].trim().split(" ");
                    String personName = currentPredicate[0].trim();
                    String relation = UploadData.generate(currentPredicate[1].trim());
                    String value = currentPredicate[2].trim();

                    if (value.charAt(0) == '{')
                        value = UploadData.generate(value);

                    if (relation.equals(String.valueOf(RelationEnum.FRIEND))) {
                        if (value.charAt(0) != '$')
                            value = UploadData.generate(value);
                    }

                    if (personName.charAt(0) != '$')
                        personName = UploadData.generate(personName);

                    predicate = new Predicate(personName, relation, value);
                    predicateList.add(predicate);

                }
                queryModel.setPredicates(predicateList);
                queryModel.setProjections(projects);
                queries.add(queryModel);
                processQueries(queries);
            }
            queries.forEach(query1 -> System.out.println(query1.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processQueries(List<Query> queries) {

        for (int i = 0; i < queries.size(); i++) {
            Query currentQuery = queries.get(i);
            List<Predicate> currentPredicateList = currentQuery.getPredicates();
            List<Person> result = new CopyOnWriteArrayList<>();
            List<String> res = new ArrayList<>();
            List<String> projections = currentQuery.getProjections();

            Set<Person> friends = null;

            for (int k = 0; k < currentPredicateList.size(); k++) {
                Predicate predicate = currentPredicateList.get(k);

                String first = predicate.getPersonName();
                String relation = predicate.getRelation();
                String second = predicate.getValue();


                List<Person> personList = null;

                if (null != friends && !friends.isEmpty()) {
                    personList = new ArrayList<>(friends);
                } else {
                    personList = new ArrayList<>(UploadData.persons.values());
                }

                if (first.charAt(0) == '$') {
                    String var1 = null;
                    if (second.charAt(0) == '$') {

                    }
                    for (Person person : personList) {
                        var1 = person.getName();
                        if (second.charAt(0) == '$') {
                            String var2 = null;
                            if (relation.equals(FRIEND) && !person.getFriends().isEmpty()) {
                                friends = person.getFriends();
                                for (Person friend : friends) {
                                    var2 = friend.getName();
                                    res.add(var1 + "," + var2);
                                }
                            }
                        } else {
                            if (getResult(relation, person, second)) {
                                res.add(person.getName());
                            }
                        }
//                else
//                        {
//                            Person person = UploadData.persons.get(currentPredicate.getPersonName());
//                            if (relation.equals(FRIEND)) {
//                                Set<Person> friendsList = person.getFriends();
//                            } else if (relation.equals(WORK)) {
//                                if (value.equals(person.getWorkLocation())) {
//                                    result.add(person);
//                                }
//                            } else if (relation.equals(STAY)) {
//                                if (value.equals(person.getStaysAt()))
//                                    result.add(person);
//                            } else if (relation.equals(STUDY)) {
//                                if (value.equals(person.getStudiedAt()))
//                                    result.add(person);
//                            } else if (relation.equals(PLAY)) {
//                                //TODO
//                                person.getHobbies();
//                            }
//                        } else{

                        if (relation.equals(FRIEND)) {
                            if (UploadData.persons.get(first).getFriends().isEmpty()) {
                                break;
                            }
                            friends = UploadData.persons.get(first).getFriends();
                        }
                    }
                }
            }
        }
    }

    private boolean getResult(String relation, Person person, String value) {
        if (relation.equals(WORK) && value.equals(person.getWorkLocation())) {
            return true;
        } else if (relation.equals(STAY) && value.equals(person.getStaysAt())) {
            return true;
        } else if (relation.equals(STUDY) && value.equals(person.getStudiedAt())) {
            return true;
        } else if (relation.equals(PLAY) && person.getHobbies().contains(value)) {
            return true;
        }
        return false;
    }
}
