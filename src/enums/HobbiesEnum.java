package enums;

import java.util.EnumSet;

public enum HobbiesEnum {

    CRICKET("Cricket"), CHESS("Chess");


    private String value;

    private HobbiesEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public static final EnumSet<HobbiesEnum> all = EnumSet.of(CRICKET, CHESS);

    public static HobbiesEnum getHobiesType(String requestType) {
        HobbiesEnum[] var1 = values();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            HobbiesEnum l = var1[var3];
            if (l.value.equalsIgnoreCase(requestType)) {
                return l;
            }
        }
        return null;
    }
}
