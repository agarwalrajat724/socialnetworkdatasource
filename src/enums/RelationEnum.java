package enums;

import java.util.EnumSet;

public enum  RelationEnum {
    FRIEND("isFriendOf"), WORK("worksAt"), STAY("staysIn"), STUDY("studiedAt"), PLAY("plays");

    private String value;

    RelationEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String toString() {
        return value;
    }

    public static void main(String[] args) {
        System.out.println(RelationEnum.FRIEND.getValue());
        System.out.println(String.valueOf(RelationEnum.FRIEND));
    }

    public static final EnumSet<RelationEnum> all = EnumSet.of(FRIEND, WORK, STAY, STUDY, PLAY);

    public static RelationEnum getRelationType(String requestType) {
        RelationEnum[] var1 = values();
        int var2 = var1.length;

        for(int var3 = 0; var3 < var2; ++var3) {
            RelationEnum l = var1[var3];
            if (l.value.equalsIgnoreCase(requestType)) {
                return l;
            }
        }
        return null;
    }
}
