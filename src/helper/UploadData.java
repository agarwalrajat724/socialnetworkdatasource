package helper;

import enums.HobbiesEnum;
import enums.RelationEnum;
import junit.framework.TestCase;
import model.Person;
import model.Predicate;
import model.Query;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import static Constants.RelationConstants.*;
import static Constants.RelationConstants.PLAY;

public class UploadData {

    private BufferedReader bufferedReader = null;

    public static Map<String, Person> persons = null;

    private static final String FRIEND = "isFriendOf";

    private static final String WORK = "worksAt";

    private static final String STAY = "staysIn";

    private static final String STUDY = "studiedAt";

    private static final String PLAY = "plays";

    private static final String SELECT = "SELECT";

    private static final String WHERE = "WHERE";

    @Before
    public void setUp() {
        try {
            bufferedReader = new BufferedReader(new FileReader("/home/raagar/Documents/DataSource/input.txt"));
            persons = new ConcurrentHashMap<>();
            String line = null;

            while ((line = bufferedReader.readLine()) != null) {
                processData(line);
            }

            bufferedReader = new BufferedReader(new FileReader("/home/raagar/Documents/DataSource/Query.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void processData(String line) {
        String[] data = line.split(",");

        String name = generate(data[0].trim());

        String type = generate(data[1].trim());

        String secondPart = generate(data[2].trim());

        Person person = persons.getOrDefault(name, new Person(name));

        switch (type) {
            case WORK:
                person.setWorkLocation(secondPart);
                break;

            case STAY:
                person.setStaysAt(secondPart);
                break;

            case STUDY:
                person.setStudiedAt(secondPart);
                break;

            case PLAY:
                HobbiesEnum hobbiesEnum = HobbiesEnum.getHobiesType(secondPart);
                if (null != hobbiesEnum) {
                    person.getHobbies().add(hobbiesEnum);
                }
                break;

            case FRIEND:
                Person friend = persons.getOrDefault(secondPart, new Person(secondPart));

                persons.putIfAbsent(secondPart, friend);
                person.getFriends().add(friend);
                break;

            default:
                break;
        }

        persons.put(name, person);
    }

    public static String generate(String str) {
        return str.substring(1, str.length() - 1);
    }

    @Test
    public void firstTest() {
        System.out.println(persons);
        try {
            String query = null;
            List<Query> queries = new ArrayList<>();
            while ((query = bufferedReader.readLine()) != null) {
                String upperQuery = query.toUpperCase();
                int indexOfWhere = upperQuery.indexOf(WHERE);
                int indexOfSelect = upperQuery.indexOf(SELECT);

                if (indexOfSelect == -1) {
                    System.out.println("Error... No Select clause in the Query...");
                    return;
                }

                if (indexOfWhere == -1) {
                    System.out.println("Error... No Where clause in the Query...");
                    return;
                }

                String[] projections = query.substring(SELECT.length(), indexOfWhere).trim().split(",");
                List<String> projects = new ArrayList<>();
                if (null != projections && projections.length > 0) {
                    for (int i = 0; i < projections.length; i++) {
                        String pr = projections[i].trim();
                        projects.add(pr.substring(1, pr.length()));
                    }
                }

                String[] predicates = query.substring(indexOfWhere + WHERE.length()).split(",");

                Query queryModel = new Query();
                Predicate predicate = null;
                List<Predicate> predicateList = new ArrayList<>();
                for (int i = 0; i < predicates.length; i++) {
                    String[] currentPredicate = predicates[i].trim().split(" ");
                    String personName = currentPredicate[0].trim();
                    String relation = UploadData.generate(currentPredicate[1].trim());
                    String value = currentPredicate[2].trim();

                    if (value.charAt(0) == '{')
                        value = UploadData.generate(value);

                    if (relation.equals(String.valueOf(RelationEnum.FRIEND))) {
                        if (value.charAt(0) != '$')
                            value = UploadData.generate(value);
                    }

                    if (personName.charAt(0) != '$')
                        personName = UploadData.generate(personName);

                    predicate = new Predicate(personName, relation, value);
                    predicateList.add(predicate);

                }
                queryModel.setPredicates(predicateList);
                queryModel.setProjections(projects);
                queries.add(queryModel);
                processQueries(queries);
            }
            queries.forEach(query1 -> System.out.println(query1.toString()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void processQueries(List<Query> queries) {

        for (int i = 0; i < queries.size(); i++) {
            Query currentQuery = queries.get(i);
            List<Predicate> currentPredicateList = currentQuery.getPredicates();
            List<Person> result = new CopyOnWriteArrayList<>();
            List<String> res = new ArrayList<>();
            List<String> projections = currentQuery.getProjections();

            Set<Person> friends = null;

            for (int k = 0; k < currentPredicateList.size(); k++) {
                Predicate predicate = currentPredicateList.get(k);

                String first = predicate.getPersonName();
                String relation = predicate.getRelation();
                String second = predicate.getValue();


                List<Person> personList = null;

                if (null != friends && !friends.isEmpty()) {
                    personList = new ArrayList<>(friends);
                } else {
                    personList = new ArrayList<>(UploadData.persons.values());
                }

                if (first.charAt(0) == '$') {
                    String var1 = null;
                    if (second.charAt(0) == '$') {

                    }
                    for (Person person : personList) {
                        var1 = person.getName();
                        if (second.charAt(0) == '$') {
                            String var2 = null;
                            if (relation.equals(FRIEND) && !person.getFriends().isEmpty()) {
                                friends = person.getFriends();
                                for (Person friend : friends) {
                                    var2 = friend.getName();
                                    res.add(var1 + "," + var2);
                                }
                            }
                        } else {
                            if (getResult(relation, person, second)) {
                                res.add(person.getName());
                            }
                        }
                    }
                } else {

                    if (relation.equals(FRIEND)) {
                        if (UploadData.persons.get(first).getFriends().isEmpty()) {
                            break;
                        }
                        friends = UploadData.persons.get(first).getFriends();
                    }
                }

//                while (!result.isEmpty()) {
//                    Person person = result.remove(0);
//                    if (getResult(predicate.getRelation(), person, predicate.getValue(), result)) {
//                        result.add(person);
//                    }
//                }
//
//                if (predicate.getPersonName().charAt(0) != '$') {
//                    Person person = UploadData.persons.get(predicate.getPersonName());
//                    if (predicate.getRelation().equals(FRIEND) && !person.getFriends().isEmpty()) {
//                        result.addAll(person.getFriends());
//                    }
//                } else {
//                    for (Map.Entry<String, Person> entry : UploadData.persons.entrySet()) {
//
//                    }
//                }
            }

//            if (!result.isEmpty()) {
//                List<String> projections = currentQuery.getProjections();
//                int n = projections.size();
//
//            }


//            for (int j = 0; j < currentPredicateList.size(); j++) {
//                Predicate currentPredicate = currentPredicateList.get(j);
//                String relation = currentPredicate.getRelation();
//                String value = currentPredicate.getValue();
//                if (currentPredicate.getPersonName().charAt(0) == '$') {
//                    //get all persons from persons
//
//                    if (result.isEmpty()) {
//                        for (Map.Entry<String, Person> entry : UploadData.persons.entrySet()) {
//
//                            if (getResult(relation, entry.getValue(), value)) {
//                                result.add(entry.getValue());
//                            }
//                        }
//                    } else {
//
//                    }
//
//                } else {
//                    Person person = UploadData.persons.get(currentPredicate.getPersonName());
//                    getResult(relation, person);
//
//                }
//            }
        }
    }

    private boolean getResult(String relation, Person person, String value) {
        if (relation.equals(WORK) && value.equals(person.getWorkLocation())) {
            return true;
        } else if (relation.equals(STAY) && value.equals(person.getStaysAt())) {
            return true;
        } else if (relation.equals(STUDY) && value.equals(person.getStudiedAt())) {
            return true;
        } else if (relation.equals(PLAY) && person.getHobbies().contains(value)) {
            return true;
        }
        return false;
    }

    @After
    public void tearDown() {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
