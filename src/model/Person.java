package model;

import java.util.HashSet;
import java.util.Set;


/**
 *
 *
 * line	represents	two	entities	associated	with	a	given	relation.
 * {Neha}, {isFriendOf}, {Anish}
 * {Neha}, {isFriendOf}, {Akash}
 * {Anish}, {isFriendOf}, {Rajiv}
 * {Anish},	{isFriendOf}, {Sachin}
 * {Akash}, {isFriendOf}, {Sachin}
 * {Rajiv}, {worksAt}, {Infosys}
 * {Rajiv}, {staysIn}, {Pune}
 * {Rajiv}, {studiedAt}, {MIT}
 * {Sachin}, {staysIn}, {Mumbai}
 * {Sachin}, {studiedAt}, {IITB}
 * {Sachin}, {worksAt}, {Cisco}
 *
 *
 */

public class Person {

    private String name;

    private String workLocation;

    private String studiedAt;

    private String staysAt;

    private Set<String> hobbies;

    private Set<Person> friends;


    public Person(String name, Set<String> hobbies, Set<Person> friends) {
        this.name = name;
        this.hobbies = hobbies;
        this.friends = friends;
    }

    public Person(String name) {
        this.name = name;
        if (null == this.hobbies) {
            this.hobbies = new HashSet<>();
        }

        if (null == this.friends) {
            this.friends = new HashSet<>();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkLocation() {
        return workLocation;
    }

    public void setWorkLocation(String workLocation) {
        this.workLocation = workLocation;
    }

    public String getStudiedAt() {
        return studiedAt;
    }

    public void setStudiedAt(String studiedAt) {
        this.studiedAt = studiedAt;
    }

    public String getStaysAt() {
        return staysAt;
    }

    public void setStaysAt(String staysAt) {
        this.staysAt = staysAt;
    }

    public Set<String> getHobbies() {
        return hobbies;
    }

    public void setHobbies(Set<String> hobbies) {
        this.hobbies = hobbies;
    }

    public Set<Person> getFriends() {
        return friends;
    }

    public void setFriends(Set<Person> friends) {
        this.friends = friends;
    }
}
