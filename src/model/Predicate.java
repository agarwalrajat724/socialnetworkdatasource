package model;

public class Predicate {

    private String personName;
    private String relation;
    private String value;

    public Predicate(String personName, String relation, String value) {
        this.personName = personName;
        this.relation = relation;
        this.value = value;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Predicate{" +
                "personName='" + personName + '\'' +
                ", relation='" + relation + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
