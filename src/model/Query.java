package model;

import java.util.ArrayList;
import java.util.List;

public class Query {

    private List<String> projections;

    private List<Predicate> predicates;

    public Query() {

        if (this.projections == null) {
            this.projections = new ArrayList<>();
        }

        if (null == this.predicates) {
            this.predicates = new ArrayList<>();
        }
    }

    public List<String> getProjections() {
        return projections;
    }

    public void setProjections(List<String> projections) {
        this.projections = projections;
    }

    public List<Predicate> getPredicates() {
        return predicates;
    }

    public void setPredicates(List<Predicate> predicates) {
        this.predicates = predicates;
    }

    @Override
    public String toString() {
        return "Query{" +
                "projections=" + projections +
                ", predicates=" + predicates +
                '}';
    }
}
